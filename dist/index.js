"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _ambtecLogger = _interopRequireDefault(require("ambtec-logger"));

var _Iam = _interopRequireDefault(require("./libs/Iam"));

// @ts-ignore
module.exports = RED => {
  "use strict";

  const clientSecret = "";
  const iam = new _Iam.default();
  /**
   * Handle iam logout event
   *
   * @param {*} config
   */

  function IamLogin(config) {
    const node = this;
    RED.nodes.createNode(node, config);
    node.on("input", msg => {
      // Prepare base configuration
      const iamConfig = {
        grant_type: null,
        client_id: msg.payload.client_id || null,
        token: msg.payload.token || null,
        username: msg.payload.username || null,
        password: msg.payload.password || null,
        code: msg.payload.code || null,
        redirect_uri: msg.payload.redirect_uri || null,
        scope: null
      }; // If authorization header isset

      if (msg.req?.headers?.authorization) {
        if (msg.req.headers.authorization.indexOf("Basic") > -1) {
          const base64 = msg.req.headers.authorization.replace("Basic ", "");
          const buff = Buffer.from(base64, "base64");
          const text = buff.toString("utf-8");
          const credentials = text.split(":");

          if (2 === credentials.length) {
            iamConfig.username = credentials[0];
            iamConfig.password = credentials[1];
          }
        }

        if (msg.req.headers.authorization.indexOf("Bearer") > -1) {
          iamConfig.token = msg.req.headers.authorization.replace("Bearer ", "");
        }
      } // Prepare request: Code, Token, User+PW


      let payload;
      let authType = "";

      if (iamConfig.code) {
        authType = "code";
        payload = {
          grant_type: "authorization_code",
          client_id: `${iamConfig.client_id}`,
          client_secret: `${clientSecret}`,
          code: `${iamConfig.code}`,
          redirect_uri: `${iamConfig.redirect_uri}`,
          scope: "openid"
        };
      } else if (iamConfig.token) {
        authType = "token";
        payload = {
          grant_type: "refresh_token",
          client_id: `${iamConfig.client_id}`,
          client_secret: `${clientSecret}`,
          refresh_token: `${iamConfig.token}`,
          redirect_uri: `${iamConfig.redirect_uri}`,
          scope: "openid"
        };
      } else {
        authType = "user+pw";
        payload = {
          grant_type: "password",
          client_id: `${iamConfig.client_id}`,
          client_secret: `${clientSecret}`,
          username: `${iamConfig.username}`,
          password: `${iamConfig.password}`,
          redirect_uri: `${iamConfig.redirect_uri}`,
          scope: "openid"
        };
      } // Make request


      iam.send(config.endpoint, payload).then(res => {
        if (400 <= res.statusCode) {
          _ambtecLogger.default.error({
            breadcrumbs: ["iam", authType, "login"],
            message: "Response with error",
            properties: res
          });

          node.send([null, msg]);
          return;
        }

        _ambtecLogger.default.debug({
          breadcrumbs: ["iam", authType, "login"],
          message: "Account logged in",
          properties: {
            httpStatusCode: res.statusCode,
            data: res.data
          }
        }); // Combine parts of configuration and response so we have it in the output


        msg.iam = iam.prepareIamNodeBody({ ...{
            client_id: iamConfig.client_id
          },
          ...res.data
        });
        node.send([msg, null]);
      }).catch(error => {
        _ambtecLogger.default.error({
          breadcrumbs: ["iam", authType, "login"],
          message: "Request error happen",
          properties: error
        });

        node.send([null, msg]);
      });
    });
  }

  RED.nodes.registerType("iam-login", IamLogin);
  /**
   * Handle iam logout event
   *
   * @param {*} config
   */

  function IamLogout(config) {
    const node = this;
    RED.nodes.createNode(node, config);
    node.on("input", msg => {
      const payload = {
        grant_type: "refresh_token",
        client_id: "account",
        client_secret: `${clientSecret}`,
        refresh_token: `${msg.iam.token}`,
        scope: "openid"
      };
      iam.send(config.endpoint, payload).then(res => {
        _ambtecLogger.default.debug({
          breadcrumbs: ["iam", "logout"],
          message: "Account logged out",
          properties: {
            httpStatusCode: res.statusCode,
            data: res.data
          }
        });

        delete msg.iam;
        node.send([msg, null]);
      }).catch(error => {
        _ambtecLogger.default.error({
          breadcrumbs: ["iam", "logout"],
          message: "Request error happen",
          properties: error
        });

        node.send([null, msg]);
      });
    });
  }

  RED.nodes.registerType("iam-logout", IamLogout);
  /**
   * Handle iam logout event
   *
   * @param {*} config
   */

  function IamHasGroup(config) {
    const node = this;
    RED.nodes.createNode(node, config);
    node.on("input", msg => {
      if (!msg.iam || !msg.iam.groups) {
        node.send([null, msg]);
        return;
      }

      const hasGroup = msg.iam.groups.indexOf(config.group) > -1;

      if (hasGroup) {
        node.send([msg, null]);
        return;
      }

      node.send([null, msg]);
    });
  }

  RED.nodes.registerType("iam-hasgroup", IamHasGroup);
};